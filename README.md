to-activate
===========

simple application which server the role of a simple XDG app. Should be activated
by either qtshell or by activator (another application which binds to agl-shell-desktop).
