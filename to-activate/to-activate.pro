TEMPLATE = app
TARGET = to-activate

QT = qml quick
CONFIG += c++11 link_pkgconfig
#DESTDIR = .
DESTDIR = $${OUT_PWD}/../package/root/bin

SOURCES += src/main.cpp
RESOURCES += qml/qml.qrc
