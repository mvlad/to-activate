#include <QGuiApplication>
#include <QCommandLineParser>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlComponent>
#include <QtQml/qqml.h>
#include <QWindow>
#include <QQuickWindow>
#include <QScreen>
#include <QTimer>

int main(int argc, char *argv[])
{
	setenv("QT_QPA_PLATFORM", "wayland", 1);
	QCommandLineParser parser;
	QGuiApplication a(argc, argv);

	parser.addPositionalArgument("appid", a.translate("main", "application id"));
	parser.addHelpOption();
	parser.addVersionOption();
	parser.process(a);
	QStringList positionalArguments = parser.positionalArguments();

	if (positionalArguments.length() == 1) {
		// xdg_toplevel.set_app_id()
		a.setDesktopFileName(positionalArguments.takeFirst());
	}

	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

	return a.exec();
}
